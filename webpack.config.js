const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');

const isDevelopment = process.env.NODE_ENV === 'development';

const extractSass = new ExtractTextPlugin({
    filename: '[name].[contenthash].css',
    disable: isDevelopment
});

module.exports = {
    entry: [
        './src/app/main.js',
        './src/app/styles/main.scss'
    ],
    output: {
        filename: '[name].[chunkhash].js',
        path: __dirname + '/dist'
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    presets: ['env', 'es2015', 'react']
                }
            },
            {
                test: /\.css$/,
                loader: extractSass.extract({ fallback: "style-loader", use : ["css-loader"] })
            },
            {
                test: /\.scss$/,
                loader: extractSass.extract({ fallback: "style-loader", use : ["css-loader", "sass-loader"] })
            },
            {
                test: /\.less$/,
                loaders: ['style', 'css', 'less']
            },
            {
                test: /\.woff$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]"
            },
            {
                test: /\.woff2$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]"
            },
            {
                test: /\.(svg|gif|png|jpg|jpeg)$/,
                loader: "file-loader" + (isDevelopment ? '' : "?publicPath=./&name=images/[hash].[ext]")
            },
            {
                test: /\.(eot|ttf)$/,
                loader: "file-loader" + (isDevelopment ? '' : "?publicPath=./&name=fonts/[hash].[ext]")
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({template: './index.html'}),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        extractSass
    ],
    resolve: {
        alias: {
            components: path.resolve(__dirname, './src/app/components/'),
            containers: path.resolve(__dirname, './src/app/containers/'),
            services: path.resolve(__dirname, './src/app/services/'),
            styles: path.resolve(__dirname, './src/app/styles/'),
            images: path.resolve(__dirname, './src/app/images/'),
            config: path.resolve(__dirname, './src/app/config/')
        }
    }
};