# React/Redux Starter Kit with ES6, Babel, Sass, Bootstrap and WebPack

### Technologies
- Javascript ES6
- React
- Redux
- Webpack
- Babel
- Npm
- Bootstrap

### Installation
- **npm install** installs dependencies

### Development
- **npm run dev** : Serves project at http://localhost:9000

### Production
- **npm run prod** : Exports everything to production ready 'dist' folder